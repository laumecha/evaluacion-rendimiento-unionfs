#!/usr/bin/env python3
# Fichero que sirve para ejecutar test de las capas de lecura
# Se realizan dos test de lectura y escritura, una sobre la capa 1 y otra sobre la 10 (mas o menos) 
# Guarda los resultados en final_results/STORAGE_DRIVERS/layers_r_few_and_multiple
#
import os
import re
import subprocess
import sys
import time
from functools import lru_cache


STORAGE_DRIVERS = 'overlay2'
PATH='layers_r_few_and_multiple'
NUM_RUNS_PER_TEST = 1
NUM_INSTANCES = (1,2)
LAYERS = ('01', '10')
TYPE_FILES = ('small-files', 'big-files')
def write_result(test_name, result):
    subprocess.check_call(('rm', '-rf', 'results/*'))
    os.makedirs('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=PATH,
        ), exist_ok=True)
    with open(os.path.join('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=PATH,
        ),test_name), 'a') as f:
        print(result, file=f)

def run_tests(tests, num_runs=NUM_RUNS_PER_TEST, num_instances_scenarios=NUM_INSTANCES, layers=LAYERS, type_files=TYPE_FILES):
    print('[info] FORMAT: name_test_script.num_iteration.storage_driver.test_layer.type_file')
    for run in range(num_runs):
        #clean container
        subprocess.check_call(('sh', 'setup.sh', 'dockerfiles/Dockerfile_r_layers'))
        for num_instances in num_instances_scenarios:
            print('tests: {}'.format(tests))
            for test in tests:
                for layer in layers:
                    for type_file in type_files:
                        test_name = '{type}.{test}.{num_instances}.{storage_driver}.{layer}.{type_file}'.format(
                            type='test_r_layers',
                            test=test,
                            num_instances=num_instances,
                            storage_driver=STORAGE_DRIVERS,
                            layer=layer,
                            type_file=type_file,
                        )
                        print('running test: {}'.format(test_name))
                        print('{test} in /home/{layer}/{type_file}'.format(
                                            test=test,
                                            layer=layer,
                                            type_file=type_file,
                                        ))

                        start = time.time()
                        procs = [
                            subprocess.Popen(
                                (
                                    (
                                        'docker', 'exec', '-it',
                                        'con_test', '/bin/sh',
                                        '/home/test_scripts/{}'.format(test), '/home/test_r_layers/{layer}/{type_file}'.format(
                                            layer=layer,
                                            type_file=type_file,
                                        )
                                    )
                                )
                            )
                        ]

                        while procs:
                            proc = procs.pop()
                            assert proc.wait() == 0, proc.returncode

                        duration = time.time() - start
                        print('Took {} seconds.'.format(duration))

                        write_result(test_name, duration)


def all_tests():
    return sorted(test for test in os.listdir('test_scripts') if not test.startswith('.'))


def main(argv=None):
    run_tests(all_tests())

    #Esto es para forzar la prueba de primero la escritura y luego la lectura
    #test_o=('write_script.sh','read_script.sh')
    #run_tests(test_o)

if __name__ == '__main__':
    sys.exit(main())
