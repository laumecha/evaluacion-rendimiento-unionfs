Para llevar a cabo el test:

1. sh rm_setup.sh
2. ./<cualquier_test>.py

Todos los test realizan internamente la creación de los recursos necesarios.

Archivos:
·test_wr_num_capas.py: realiza los test sobre las capas de escritura.

·test_r_num_capas: realiza los test sobre las capas de lectura.

·test_layers_and_volume.py: realiza los test sobre las capas de escritura y capas de lectura así como de los volumes.

·media.py: calcula la media de los ficheros obtenidos de los test (para poder hacer la prueba sobre los volumes se necesita ejecutar como sudo)
