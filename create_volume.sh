#!/bin/bash
volume_name=$1
volume_host_path='/var/lib/docker/volumes'
#si no existe crear volume
if [ !"$(docker volume ls -q -f name=$volume_name)" ]; then
    echo "[info]creating volume..."
    docker volume create $volume_name > /dev/null 2>&1
    echo "[info]creating directories inside volume from host..."
    mkdir $volume_host_path/$volume_name/_data/big-files
    mkdir $volume_host_path/$volume_name/_data/small-files

    echo "[info]creating files inside volume from host..."
    # Make a bunch of big files.
    for i in $(seq 0 9); do \
        for size in 1M 8M 32M 128M; do \
            dd if=/dev/urandom of=$volume_host_path/$volume_name/_data/big-files/${size}-${i}.bin bs=${size} count=1 2>/dev/null; \
        done; \
    done

    # And a lot more small files.
    for i in $(seq 0 1000); do \
        dd if=/dev/urandom of=$volume_host_path/$volume_name/_data/small-files/$i bs=512 count=1 2>/dev/null; \
    done
    echo "[info]VOLUME AND FILES CREATED!"
fi