#!/bin/bash
im_name='test:test'
con_name='con_test'
volume_name='vol_test'
dockerfile_path='.'

#borrar contenedor si existe
if [ "$(docker ps -a -q -f name=$con_name)" ]; then
    echo "[info] removing existing contaner..."
    docker stop $con_name > /dev/null 2>&1
    docker rm $con_name > /dev/null 2>&1
fi

if [ "$(docker volume ls -q -f name=$volume_name)" ]; then
    echo "[info] removing existing volume..."
    docker volume rm $volume_name > /dev/null 2>&1
fi

if [ "$(docker images -q $im_name)" ]; then
    echo "[info] removing existing image..."
    docker rmi $im_name > /dev/null 2>&1
fi

echo "[info] ALL DONE"