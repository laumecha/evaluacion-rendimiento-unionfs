#!/usr/bin/env python3
# Fichero que sirve para ejecutar test de las capas de lecura, capa escritura/lectura y los volumes
# No tiene en cuenta la profundidad de capa 
# Guarda los resultados en final_results/STORAGE_DRIVERS/layers_and_volumes_first_<w o r>
import os
import re
import subprocess
import sys
import time
from functools import lru_cache

#CAMBIAR ESTO SI ES NECESARIO
STORAGE_DRIVERS = 'overlay2'
first_read=True

NUM_RUNS_PER_TEST = 1
NUM_INSTANCES = (1,2)
LAYERS = ('test_wr_layers','test_r_layers','test_volumes')
TYPE_FILES = ('small-files', 'big-files')
def write_result(test_name, result):
    path='layers_and_volumes_first_w'
    if first_read:
        path='layers_and_volumes_first_r'
    print('write innn {}'.format(path))
    subprocess.check_call(('rm', '-rf', 'results/*'))
    os.makedirs('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=path,
        ), exist_ok=True)
    with open(os.path.join('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=path,
        ),test_name), 'a') as f:
        print(result, file=f)

def run_tests(tests, num_runs=NUM_RUNS_PER_TEST, num_instances_scenarios=NUM_INSTANCES, layers=LAYERS, type_files=TYPE_FILES):
    print('[info] FORMAT: name_test_script.num_iteration.storage_driver.test_layer.type_file')
    for run in range(num_runs):
        #clean container
        subprocess.check_call(('sh', 'setup.sh', 'dockerfiles/Dockerfile'))
        for num_instances in num_instances_scenarios:
            print('tests: {}'.format(tests))
            for test in tests:
                for layer in layers:
                    for type_file in type_files:
                        test_name = '{test}.{num_instances}.{storage_driver}.{layer}.{type_file}'.format(
                            test=test,
                            num_instances=num_instances,
                            storage_driver=STORAGE_DRIVERS,
                            layer=layer,
                            type_file=type_file,
                        )
                        print('running test: {}'.format(test_name))
                        print('{test} in /home/{layer}/{type_file}'.format(
                                            test=test,
                                            layer=layer,
                                            type_file=type_file,
                                        ))

                        start = time.time()
                        procs = [
                            subprocess.Popen(
                                (
                                    (
                                        'docker', 'exec', '-it',
                                        'con_test', '/bin/sh',
                                        '/home/test_scripts/{}'.format(test), '/home/{layer}/{type_file}'.format(
                                            layer=layer,
                                            type_file=type_file,
                                        )
                                    )
                                )
                            )
                        ]

                        while procs:
                            proc = procs.pop()
                            assert proc.wait() == 0, proc.returncode

                        duration = time.time() - start
                        print('Took {} seconds.'.format(duration))

                        write_result(test_name, duration)


def all_tests():
    return sorted(test for test in os.listdir('test_scripts') if not test.startswith('.'))


def main(argv=None):
    if first_read:
        run_tests(all_tests())
    else:
    #Esto es para forzar la prueba de primero la escritura y luego la lectura
        test_o=('write_script.sh','read_script.sh')
        run_tests(test_o)

if __name__ == '__main__':
    sys.exit(main())