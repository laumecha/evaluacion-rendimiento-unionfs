#!/usr/bin/env python3
import os
import re
import subprocess
import sys
import time
from functools import lru_cache
STORAGE_DRIVERS = 'aufs'


def write_result(test_name, result):

    #os.makedirs('{}'.format(test_name), exist_ok=True)
    with open(os.path.join('{}'.format(test_name)), 'a') as f:
        print(result, file=f)

def run_media(tests):
    for test in tests:
        files=sorted(test for test in os.listdir('final_results/{storage}/{dir}'.format(
            storage=STORAGE_DRIVERS,
            dir=test,
            )) if not test.startswith('.'))
        for file in files:
            media=0
            cont = 0;
            with open('resultados/{storage}/{dir}/{f}'.format(
            storage=STORAGE_DRIVERS,
            dir=test,
            f=file,
            ), "r") as line:
                for contenido in line:
                #contenido=line.read()
                    media = media+float(contenido)
                    cont=cont+1
                    final=media/cont
                write_result('resultados/{storage}/{dir}/{f}'.format(
                    storage=STORAGE_DRIVERS,
                    dir=test,
                    f=file,
                    ), final)

def all_results():
    return sorted(test for test in os.listdir('final_results/{}'.format(STORAGE_DRIVERS)) if not test.startswith('.'))

def main(argv=None):
    run_media(all_results())

if __name__ == '__main__':
    sys.exit(main())