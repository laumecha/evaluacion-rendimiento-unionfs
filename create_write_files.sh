#!/bin/bash
#con_name=$1
path='/home/test_wr_layers'
echo "[info]creating directories inside container from container..."
#docker attach $con_name
for i in $(seq 0 9); do \
    for size in 1M 8M 32M 128M; do \
        dd if=/dev/urandom of=$path/big-files/${size}-${i}.bin bs=${size} count=1 2>/dev/null; \
    done; \
done
# And a lot more small files.
for i in $(seq 0 1000); do \
    dd if=/dev/urandom of=$path/small-files/$i bs=512 count=1 2>/dev/null; \
done
echo "[info]FILES CREATED!"