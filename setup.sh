#!/bin/bash

im_name='test:test'
con_name='con_test'
volume_name='vol_test'
volume_path='/home/test_volumes'
dockerfile_path=$1

#si no existe crear imagen
if [ ! "$(docker images -q $im_name)" ]; then
    echo "[info] creating image..."
    docker build -t $im_name -f $dockerfile_path .
    echo "[info] IMAGE CREATED!"
fi
#si no existe crear volume
if [ ! "$(docker volume ls -q -f name=$volume_name)" ]; then
    sh create_volume.sh $volume_name
fi

#si no existe crear contenedor
if [ "$(docker ps -a -q -f name=$con_name)" ]; then
    # rm container if exist
    echo "[info] removing existing contaner..."
    docker stop $con_name > /dev/null 2>&1
    docker rm $con_name > /dev/null 2>&1
fi
echo "[info] creating container..."
docker run --privileged -i -t -d -v $volume_name:$volume_path --name=$con_name $im_name /bin/bash > /dev/null 2>&1
echo "[info] CONTAINER CREATE!"
#crear ficheros en la capa de lectura-escritura
echo "[info] creating files inside the container from container..."
docker exec -it $con_name /bin/sh /home/test_wr_layers/create_write_files.sh 