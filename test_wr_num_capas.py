#!/usr/bin/env python3
# Fichero que sirve para ejecutar test de las capa escritura/lectura 
# Si tiene en cuenta la profundidad de capa 
# Guarda los resultados en final_results/STORAGE_DRIVERS/layers_rw_<few o >
#
import os
import re
import subprocess
import sys
import time
from functools import lru_cache

few_layers=True

NUM_RUNS_PER_TEST = 1
NUM_INSTANCES = (1,2)
#NUM_INSTANCES = (1, 5, 10, 50, 100)
STORAGE_DRIVERS = 'aufs'
LAYERS = ('test_r_layers')
TYPE_FILES = ('small-files', 'big-files')
def write_result(test_name, result):
    path='layers_rw_multiples'
    if few_layers:
        path='layers_rw_few'
    print('write in {}'.format(path))
    subprocess.check_call(('rm', '-rf', 'results/*'))
    os.makedirs('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=path,
        ), exist_ok=True)
    with open(os.path.join('final_results/{storage}/{path}'.format(
        storage=STORAGE_DRIVERS,
        path=path,
        ),test_name), 'a') as f:
        print(result, file=f)

def run_tests(tests, num_runs=NUM_RUNS_PER_TEST, num_instances_scenarios=NUM_INSTANCES, layers=LAYERS, type_files=TYPE_FILES):
    print('[info] FORMAT: name_test_script.num_iteration.storage_driver.test_layer.type_file')
    for run in range(num_runs):
        #clean container
        if few_layers:
            subprocess.check_call(('sh', 'setup.sh', 'dockerfiles/Dockerfile_w_r_layers')) #creamos imagen con pocas capas
        else:
            subprocess.check_call(('sh', 'setup.sh', 'dockerfiles/Dockerfile_r_layers')) #creamos imagen con muchas capas
        for num_instances in num_instances_scenarios:
            print('tests: {}'.format(tests))
            for test in tests:
                    for type_file in type_files:
                        test_name = '{type}.{test}.{num_instances}.{storage_driver}.{type_file}'.format(
                            type='test_wr_layers',
                            test=test,
                            num_instances=num_instances,
                            storage_driver=STORAGE_DRIVERS,
                            type_file=type_file,
                        )
                        print('running test: {}'.format(test_name))
                        start = time.time()
                        procs = [
                            subprocess.Popen(
                                (
                                    (
                                        'docker', 'exec', '-it',
                                        'con_test', '/bin/sh',
                                        '/home/test_scripts/{}'.format(test), '/home/test_wr_layers/{type_file}'.format(
                                            type_file=type_file,
                                        )
                                    )
                                )
                            )
                        ]

                        while procs:
                            proc = procs.pop()
                            assert proc.wait() == 0, proc.returncode

                        duration = time.time() - start
                        print('Took {} seconds.'.format(duration))

                        write_result(test_name, duration)


def all_tests():
    return sorted(test for test in os.listdir('test_scripts') if not test.startswith('.'))


def main(argv=None):
    run_tests(all_tests())

if __name__ == '__main__':
    sys.exit(main())